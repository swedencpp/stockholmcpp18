#include "doctest.h"
#include <vector>
#include <algorithm>

void up_side_down (std::vector<int>& vec) {

}

std::vector<int> get_up_side_down (const std::vector<int>& vec) {
  auto out = vec ;

  return out ;
}



SCENARIO("up side down") {

  GIVEN("a collection with numbers") {

    std::vector<int> vec = {1,3,5,7} ;

    WHEN("making it up side down") {

      up_side_down(vec) ;

      THEN("the order is up side down") {
        CHECK_EQ (vec, std::vector<int>{7,5,3,1}) ;
      }
    }

    WHEN("get it up side down") {

      auto answer = get_up_side_down(vec) ;

      THEN("the order is up side down") {
        CHECK_EQ (answer, std::vector<int>{7,5,3,1}) ;
      }
      AND_THEN("the original is unchanged") {
        CHECK_EQ (vec, std::vector<int>{1,3,5,7}) ;
      }
    }

  }
}

