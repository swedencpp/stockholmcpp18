#include "doctest.h"
#include <vector>
#include <algorithm>

std::vector<int> next_bigger(std::vector<int> vec) {

}


SCENARIO("re arrange to make a bigger range") {

  GIVEN("a vector with different elements") {
    std::vector<int> vec{1,2,3} ;

    WHEN("requesting the next rearrangement of elements that is bigger") {

      auto v1 = next_bigger(vec) ;

      THEN("v1 is bigger than vec") {
        CHECK_GT ( v1, vec ) ;
        CHECK_EQ ( v1 , std::vector<int>{1,3,2} );

        AND_WHEN ("requesting the next bigger range of the bigger range") {
          auto v2 = next_bigger(v1) ;
          THEN( "we get an even bigger range"){
            CHECK_GT ( v2, v1 ) ;
            CHECK_EQ (v2 , std::vector<int>{2,1,3} );
          }
        }
      }
    }
  }
}

