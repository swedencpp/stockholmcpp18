#include "doctest.h"
#include <vector>
#include <algorithm>

void less_than_5_in_front (std::vector<int>& vec) {

}




SCENARIO("less 5 to the front") {

  GIVEN("a collection with numbers") {

    std::vector<int> vec = {7, 1, 3, 5, 4, 6, 1} ;

    WHEN("moving numbers less than 5 to the front") {

      auto copy = vec;
      less_than_5_in_front(vec) ;

      THEN("the first 4 numbers are less than 5, the others not") {
        CHECK_LT (vec[0], 5) ;
        CHECK_LT (vec[1], 5) ;
        CHECK_LT (vec[2], 5) ;
        CHECK_LT (vec[3], 5) ;
        REQUIRE ( copy.size() == vec.size()) ;
        CHECK (std::is_permutation(vec.begin(), vec.end(),
                                   copy.begin())) ;
      }
    }
  }
}

