cmake_minimum_required(VERSION 3.6)
set (CMAKE_USER_MAKE_RULES_OVERRIDE "${CMAKE_CURRENT_LIST_DIR}/CompilerOptions.cmake")
project(meetup12)


set(CMAKE_CXX_STANDARD 11)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
enable_testing()

add_library(testmain OBJECT testmain.cpp)

macro(_add_test fname)
    get_filename_component(bname ${fname} NAME_WE)
    add_executable(${bname} ${fname} $<TARGET_OBJECTS:testmain>)

    target_compile_options(${bname} 
                        PRIVATE
                        $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:GNU>>:-Wall -Wextra -pedantic>
                        $<$<CXX_COMPILER_ID:MSVC>:/W4>)

    add_test( NAME ${bname} COMMAND ${bname} )
    set_tests_properties(${bname}
        PROPERTIES
        TIMEOUT 1
        WILL_FAIL false
    )
endmacro(_add_test)

_add_test(task1.cpp)
_add_test(task2.cpp)
_add_test(task3.cpp)
_add_test(task4.cpp)
_add_test(task5.cpp)
_add_test(task6.cpp)
_add_test(task7.cpp)
_add_test(task8.cpp)
_add_test(task9.cpp)
_add_test(task10.cpp)
_add_test(task11.cpp)
_add_test(task12.cpp)
_add_test(task13.cpp)
_add_test(task14.cpp)
_add_test(task15.cpp)
_add_test(task16.cpp)
_add_test(task17.cpp)
_add_test(task18.cpp)
_add_test(task19.cpp)
_add_test(task20.cpp)
_add_test(task21.cpp)
_add_test(task22.cpp)
_add_test(task23.cpp)
_add_test(task24.cpp)