
#include "doctest.h"
#include <vector>
#include <algorithm>
#include <iterator>

std::vector<int> common_elements(const std::vector<int>& vec1,
                              const std::vector<int>& vec2) {
  std::vector<int> out ;


  return out ;
}


SCENARIO("put together") {

  GIVEN("two collections with ordered elements") {

    std::vector<int> vec1 = {1, 2, 3, 4, 5, 6, 8} ;
    std::vector<int> vec2 = {2, 4, 5, 7} ;

    WHEN("we look for values that occurr in both") {

      auto all = common_elements(vec1, vec2) ;

      THEN("we get the common element") {
        CHECK (all == std::vector<int>{2, 4, 5}) ;

      }
    }
  }
}

