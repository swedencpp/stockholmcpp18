#include "doctest.h"
#include <vector>
#include <algorithm>

// Extra points for mentioning Sean Parent's C++ Seasoning!
void put_the_last_2_in_front(std::vector<int>& vec) {

}

SCENARIO("put the last in from") {

  GIVEN("a collection with some elements") {

    std::vector<int> vec = {3, 1, 5, 4, 2} ;

    WHEN("moving the last 2 elements in front") {

      put_the_last_2_in_front(vec) ;

      THEN("the previously last 2 elements start now the list, followed by the rest") {
        CHECK (vec == std::vector<int>{4,2,3,1,5}) ;
      }
    }
  }


}

