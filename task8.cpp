#include "doctest.h"
#include <vector>
#include <algorithm>

bool are_basically_they_same(const std::vector<int>& vec1,
                  const std::vector<int>& vec2) {

}


// each element in 2 is in 1, but is has the double value
bool same_but_double_value(const std::vector<int>& vec1,
                  const std::vector<int>& vec2) {


}



SCENARIO("check if they are the same") {

  GIVEN("a collection with some numbers") {

    std::vector<int> vec = {3, 1, 5, 4, 2} ;

    WHEN("we copy it") {

      auto vec_cp = vec ;

      THEN("the copy is the same as the original") {
        CHECK (are_basically_they_same(vec, vec_cp)) ;
      }
    }

    AND_WHEN("we have a collection with other numbers") {

      std::vector<int> vec_other {3, 2, 5, 4, 1} ;

      THEN("it is not the same as the original") {
        CHECK_FALSE (are_basically_they_same(vec, vec_other)) ;
      }
    }

    AND_WHEN("we have a collection where each element is twice as big as in the original") {

      std::vector<int> vec_other = {6, 2, 10, 8, 4} ;

      THEN("they are isomorphic") {
        CHECK (same_but_double_value(vec, vec_other)) ;
      }
    }

    AND_WHEN("we have a collection where all elements but one, are as big as in the original") {

      std::vector<int> vec_other = {6, 4, 10, 9, 3} ;

      THEN("they are not isomorphic") {
        CHECK_FALSE (same_but_double_value(vec, vec_other)) ;
      }
    }

  }


}

