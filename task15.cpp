
#include "doctest.h"
#include <vector>
#include <algorithm>

// c++ 11 , 1 helper variable is allowed
// c++ 14 , no variable
void fill_with_even_numbers(std::vector<int>& vec) {

}


SCENARIO("make even numbers") {

  GIVEN("a collection with 5 zeroes") {

    std::vector<int> vec = {0, 0, 0, 0, 0} ;

    WHEN("we put in even numbers from 0") {

      fill_with_even_numbers(vec) ;

      THEN("we get the values 0,2,4,6,8") {
        std::vector<int> wanted = {0, 2, 4, 6, 8} ;
        CHECK_EQ (vec, wanted) ;
      }
    }
  }


}

