
#include "doctest.h"
#include <vector>
#include <algorithm>


void zero_all_out (std::vector<int>& vec) {
  
}


SCENARIO("zero all out") {

  GIVEN("a collection of numbers") {

    std::vector<int> vec = {1,2,3,4} ;

    WHEN("zero out all the numbers") {

      zero_all_out(vec) ;

      THEN("the collection contains only 0 values") {
        CHECK_EQ (vec, std::vector<int>{0,0,0,0}) ;
      }
    }
  }

  GIVEN("an empty collection") {

    std::vector<int> vec = {} ;

    WHEN("zero out all the numbers") {

      zero_all_out(vec) ;

      THEN("the collection is still empty") {
        CHECK (vec.empty()) ;
      }
    }
  }
}

