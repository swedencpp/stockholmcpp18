
#include "doctest.h"
#include <vector>
#include <algorithm>
#include <iterator>

std::vector<int> put_together(const std::vector<int>& vec1,
                              const std::vector<int>& vec2) {
  std::vector<int> out ;

  return out ;
}


SCENARIO("put together") {

  GIVEN("two collections, one with even numbers and one with odd numbers" ) {

    std::vector<int> even = {2, 4, 6, 8} ;
    std::vector<int> odd = {1, 3, 5, 7} ;

    WHEN("we bring them together") {

      auto all = put_together(even, odd) ;

      THEN("we have a collection with even numbers followed by odd and vice versa") {
        CHECK (all == std::vector<int>{1,2,3,4,5,6,7,8}) ;

      }
    }
  }
}

